package playground.junit.smog;

public enum AlarmLevel {
    NONE,
    INFO,
    WARNING
}
