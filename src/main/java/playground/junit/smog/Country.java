package playground.junit.smog;

public enum Country {

    ITALY,
    FINLAND,
    FRANCE,
    POLAND,
}
