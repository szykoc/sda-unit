package playground.junit.figures;

public interface Figure {
    int area();
    int circuit();
}
