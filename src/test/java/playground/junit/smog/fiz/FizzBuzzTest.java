package playground.junit.smog.fiz;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import java.time.LocalTime;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;


public class FizzBuzzTest {
    private FizzBuzz fb;

    @BeforeClass
    public static void classSetup() {
        Logger.getLogger("JUnit 5").info("Started at " + LocalTime.now());
    }

    @AfterClass
    public static void classTeardown() {
        Logger.getLogger("JUnit 5").info("Finished at " + LocalTime.now());
    }

    @Before
    public void setup() {
        Logger.getLogger("JUnit 5").info("Executing test " + LocalTime.now());
        fb = new FizzBuzz();
    }

    @Test
    public void shouldReturnFizzBuzzIfDiv3And5() {
        assertEquals("FizzBuzz", fb.fizzBuzz(15));
        assertEquals("FizzBuzz", fb.fizzBuzz(30));
        assertEquals("FizzBuzz", fb.fizzBuzz(45));
    }

    @Test
    public void shouldReturnFizzIfDiv3() {
        assertEquals("Fizz", fb.fizzBuzz(9));
        assertEquals("Fizz", fb.fizzBuzz(12));
        assertEquals("Fizz", fb.fizzBuzz(-33));
    }

    @Test
    public void shouldReturnBuzzIfDiv5() {
        assertEquals("Buzz", fb.fizzBuzz(5));
        assertEquals("Buzz", fb.fizzBuzz(55));
        assertEquals("Buzz", fb.fizzBuzz(-5555));
    }

}