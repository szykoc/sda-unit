package playground.junit.smog;

import org.junit.Test;

import static org.junit.Assert.*;

public class PmAlarmServiceTest {

    @Test
    public void shouldReturnNoneForMeasurement_10() {
        PmAlarmService service = new PmAlarmService();
        assertEquals(AlarmLevel.NONE, service.getAlarmMessage(10, Country.POLAND));
    }

    @Test
    public void shouldReturnInfoForMeasurement_201() {
        PmAlarmService service = new PmAlarmService();
        assertEquals(AlarmLevel.INFO, service.getAlarmMessage(201, Country.POLAND));
    }

    @Test
    public void shouldReturnInfoForMeasurement_350() {
        PmAlarmService service = new PmAlarmService();
        assertEquals(AlarmLevel.WARNING, service.getAlarmMessage(301, Country.POLAND));
    }
}