package playground.junit.figures;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class SquareTest {

    @Test
    public void areaOfSquare() {
        Square square = new Square(4);
        assertEquals(15, square.area());
    }
}