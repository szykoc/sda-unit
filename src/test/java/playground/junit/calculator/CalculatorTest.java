package playground.junit.calculator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

class CalculatorTest {

    @Test
    void addsTwoNumbers() {
        Calculator calculator = new Calculator();
        assertEquals(2, calculator.add(1, 1));
    }
}